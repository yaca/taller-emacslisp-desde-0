#+TITLE: Aprendiendo Emacs Lisp 1

* Notas 

Características por habilitar.

- Snippets de texto-code
- Rehacer
- números de línea
- autofill
- evaluacion de código

* Configuramos Org-mode


Se puede inicializar la configuración cargando un archivo org en *init.el*

#+BEGIN_SRC emacs-lisp
(package-initialize)
(org-babel-load-file "~/dotfiles/emacs/config.org")
#+END_SRC

Como muestran aquí:

https://ryan.himmelwright.net/post/org-babel-setup/


#+BEGIN_SRC emacs-lisp
(defun my-org-confirm-babel-evaluate (lang body) }
       (not (member lang '(
                           "bash"
                           "shell"
                           "python"
                           "awk"
                           "gnuplot"
                           "haskell"
                           "sed"
                           "javascript"
                           "go"
                           "rust"
                           "c"
                           "c++"
                           "sql"
                           "octave"
                           "dot"
                           "ditaa"
                           ))))
(setq org-babel-check-confirm-evaluate 'my-org-confirm-babel-evaluate)
(setq org-export-babel-evaluate nil)
#+END_SRC

#+RESULTS:

#+BEGIN_SRC python :results output
print("Hola")
#+END_SRC

#+RESULTS:
: Hola



#+BEGIN_SRC shell
echo "Hola chell"
#+END_SRC

#+RESULTS:
: Hola chell

* Activar Daemon Emacs.

En la terminal, registramos emacs para systemctl

#+begin_src shell
systemctl enable --user emacs
#+end_src

Activamos el emacs-server.

#+begin_src shell
systemctl start --user emacs
#+end_src

Luego, abrimos emacs como cliente.

#+begin_src shell
emacsclient -c
#+end_src


Podemos hacer un shortcut? Creando una función para *bash*,

#+begin_src shell
function cemacs(){
  emacsclient -c $@
}
#+end_src

Luego activamos algunas características necesarias:

- linum
- autofill


Para eso nos vamos al archivo de configuración base, *init.el* en el directorio *~/.emacs.d*

Al iniciar nos encontramos con un archivo /en blanco/, le enlazamos la carga de un archivo en que añadiremos nuestras configuraciones.

#+begin_src emacs-lisp
(setq custom-file (expand-file-name "custom.el" user-emacs-directory))
(when (file-exists-p custom-file)
  (load cutsom-file))
#+end_src

Según la documentación.
https://www.gnu.org/software/emacs/manual/html_node/elisp/File-Name-Expansion.html

La función *expand-file-name* toma un archivo de dirección relativa y lo convierte en absoluta.

#+begin_src shell
echo $(pwd)
#+end_src

#+RESULTS:
: /home/gnumacs/Documentos

#+begin_src python :results output
print("Hola pythoN")
#+end_src



Activamos así, en *custom.el* el sistema de paquetería *melpa*, según recomiendan en:

https://lucidmanager.org/productivity/configure-emacs/
